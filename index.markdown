---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: home
---


# BOB MOON

404-435-0391  
<bobmoon19@gmail.com>

# TECHNICAL COMMUNICATIONS LEADER
## SUMMARY
- Experienced people manager, comfortable building and leading a distributed global technical writing team and working cross-functionally to meet market release commitments.
- Highly technical, with diverse industry experience: financial services, software development, VoIP, telecommunications, hardware, high-tech manufacturing, government, office and retail systems.
- Strong writer and communicator, able to break down complex material with clarity and explain technologies in human terms.
- Passionate about people, committed ensuring a collaborative and rewarding work environment for associates and willing to provide candid feedback up and down the organization to create a better place to work.
- Committed to establishing documentation processes and reinforcing standards to ensure quality and efficiency in content development.

## TOOLS
- Content Authoring: Madcap Flare, RoboHELP, Docbook and XML authoring, FrameMaker
- E-Learning: WalkMe, Adobe Captivate, Camtasia
- Development Tools: Jira, SVN, Git, Visual Studio, Team Foundation Services, Eclipse 
- Development Languages (light coding): JavaScript, JQuery, PowerShell, Visual Basic for Applications, PHP

## EDUCATION
Bachelor of Arts in English, Cum Laude.  Berry College, Mount Berry, Georgia. Minors in Spanish and Speech/Theatre.
Certification in Knowledge Centered Service Practices (KCS v6)

 
# RECENT WORK EXPERIENCE
## Fiserv (Alpharetta, GA)

### Manager of Information Development | People management, project management.  2011 – Present.
Leading a global team of authors who create and maintain documentation for a variety of financial services software applications, including financial accounting, risk management, anti-money laundering, fraud, budgeting, wire / ACH processing, and more. 
Providing technical and strategic leadership, setting priorities and interacting with software engineering, product management, quality assurance, devops and other functional areas to ensure that our group meets a wide range of commitments for client-facing documentation.
### Documentaion Architect | Documentation design and delivery, technical writing.  2008 – 2011. 
Responsible for technology roadmap and enhancements for a team maintaining a large documentation library, while also developing a new web portal for banks and credit unions.  Designed an award-winning online tutorial, created a SharePoint content repository, and re-branded the online help and documentation.

## StarPound Technologies (Atlanta, GA) 
### Director of Technical Communications  | Technical writing, training, marketing communications.  2005 – 2008. 
Developed technical documentation for an open source VoIP technology platform utilizing single-source methodology (XML to HTML and PDF). Product library included more than 800 pages of user, programming, and technical guides. Collaborated with architects and developers to create product feature specifications, designed and delivered customer training, and created a quarterly internal newsletter.

## Words Connect (Lawrenceville, GA) 
### Principal Consultant | Freelance technical writing and marketing writing.  2002 – 2005. 
Wrote user documentation, designed Microsoft Word templates, designed web sites, wrote marketing brochure copy, and more for clients in a variety of industries: financial services, government, advertising, telecom, and manufacturing.

## RIGHTWriters (Alpharetta, GA)
### Director of Sales and Marketing  | Marketing communications, tech writing, sales, templates, and support.   2001 – 2002. 
Coordinated marketing and sales activities for a technical writing consulting company.  Responsible for brand development through marketing collateral, web site redesign, and trade show materials.  

## Rockwell Automation (Duluth, GA)
### Senior Technical Communicator / Marketing Rep  | Technical writing and marketing communications.  1998 – 2001. 
Responsible for product doc and pre-sales literature for industrial computer and monitor manufacturer, including installation manuals, data sheets, and brochures. Managed projects from conception through production, printing, distribution and translation.
Maintained two web sites for product brands and one internal support site for a global sales force.  Coordinated product launches, newsletters, and other commercial marketing activities and managed contractors and junior mar-com staff.

## Geac/Infor (Atlanta, GA)
### Technical Communications Consultant  | User documentation, online Help, and technology consulting.  1996 – 1998. 
Rewrote user documentation for mainframe general ledger system, reducing page count and dramatically improving usability. 

## ALLTEL Information Services (Atlanta, GA)
### Technical Communications Consultant  | User documentation, training development, and print production.  1994 – 1996.  
Developed training curriculum, online Help and user reference material for a cellular billing system.  

---
